# st

st is a simple terminal emulator for X which sucks less.

This is my fork of 'simple terminal' from [st.suckless.org](https://st.suckless.org/)

The following patches have been applied.

- ~~scrollback~~ We [scroll](git://git.suckless.org/scroll) now!
- alpha
- Xresources ( with an added `alpha` value )

## To add another patch
```sh
$ curl $DIFF_URL | patch -p1 --merge
```

## Minimal `.Xresources` file

```
! Color definitions
#define FG       #9a9a9a
#define BG       #000000
#define Black1   #0c0c0c
#define Black2   #282a2e
#define Red1     #88172d
#define Red2     #9c3b4e
#define Green1   #549137
#define Green2   #739f5e
#define Yellow1  #b89d6d
#define Yellow2  #a78240
#define Blue1    #5761ab
#define Blue2    #7d88ab
#define Magenta1 #513c7e
#define Magenta2 #6a5a8b
#define Cyan1    #346174
#define Cyan2    #618b98
#define White1   #aabaca
#define White2   #9a9a9a


! --------- st configuration --------- 

! Font 
st.font : inconsolata:pixelsize=12:antialias=true:autohint=true
st.scroll : scroll

! Opacity Value  [0-1]
st.alpha:      0.75


! special
st.foreground:   FG
st.background:   FG

! black
st.color0:       Black1
st.color8:       Black2

! red
st.color1:       Red1
st.color9:       Red2

! green
st.color2:       Green1
st.color10:      Green2

! yellow
st.color3:       Yellow1
st.color11:      Yellow2

! blue
st.color4:       Blue1
st.color12:      Blue2

! magenta
st.color5:       Magenta1
st.color13:      Magenta2

! cyan
st.color6:       Cyan1
st.color14:      Cyan2

! white
st.color7:       White1
st.color15:      White2
! ---------
```


# Requirements

In order to build st you need the Xlib header files.


Installation
------------
Edit config.mk to match your local setup (st is installed into
the /usr/local namespace by default).

Afterwards enter the following command to build and install st (if
necessary as root):

    make clean install


Running st
----------
If you did not install st with make clean install, you must compile
the st terminfo entry with the following command:

    tic -sx st.info

See the man page for additional details.

Credits
-------
Based on Aurélien APTEL <aurelien dot aptel at gmail dot com> bt source code.

